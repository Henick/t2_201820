package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import api.IDivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.IteradorListaDoble;

public class DivvyTripsManager implements IDivvyTripsManager {

	DoublyLinkedList<VOTrip> tripsList;
	DoublyLinkedList<VOStation> stationsList;
	
	public DivvyTripsManager() {
		tripsList=new DoublyLinkedList<VOTrip>();
		stationsList=new DoublyLinkedList<VOStation>();
	}
	
	
	public void loadStations (String stationsFile) throws IOException {
		
		File archivo=new File(stationsFile);
		FileReader fr=new FileReader(archivo);
		BufferedReader bf=new BufferedReader(fr);
		bf.readLine();
		String datos=bf.readLine();
		while(datos!=null) {
			String[] info=datos.split(",");
			int id=Integer.parseInt(info[0]);
			String name=info[1];
			String city=info[2];
			double latitude=Double.parseDouble(info[3]);
			double longitude=Double.parseDouble(info[4]);
			int capacity=Integer.parseInt(info[5]);
			Date date=new Date(info[6]);
			VOStation nuevo=new VOStation(id, name, city, latitude, longitude, capacity, date);
			stationsList.add(nuevo);
			datos=bf.readLine();
		}
		fr.close();
		bf.close();
		
	}

	
	public void loadTrips (String tripsFile) {
		File archivo=new File(tripsFile);
		FileReader fr;
		try {
			fr = new FileReader(archivo);
			BufferedReader bf=new BufferedReader(fr);
			bf.readLine();
			String datos=bf.readLine();
			while(datos!=null) {
				String[] info=datos.split(",");
				int id=Integer.parseInt(info[0]);
				Date start_time=new Date(info[1]);
				Date end_time=new Date(info[2]);
				VOByke byke=new VOByke(Integer.parseInt(info[3]));
				double duration=Double.parseDouble(info[4]);
				VOStation originStation=new VOStation(Integer.parseInt(info[5]), info[6]);
				VOStation destinationStation=new VOStation(Integer.parseInt(info[7]), info[8]);
				String usertype=info[9];
				String gender="";
				if(info.length>=11) {
					gender=info[10];
				}
				int birthyear=0;
				
				if(info.length>=12) {
					birthyear=Integer.parseInt(info[11]);
				}
				VOTrip nuevo= new VOTrip(id, start_time, end_time, byke, duration, originStation, destinationStation, usertype, gender, birthyear);
				tripsList.add(nuevo);
				
				datos=bf.readLine();
			}
			
			fr.close();
			bf.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public IDoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		
		DoublyLinkedList<VOTrip> seleccionados=new DoublyLinkedList<VOTrip>();
		
		Iterator<VOTrip> iterador= tripsList.iterator();
		while(iterador.hasNext()) {
			VOTrip actual=iterador.next();
			if(actual.getGender().equals(gender)) {
				seleccionados.add(actual);
			}
		}
		
		
		return seleccionados;
	}

	@Override
	public IDoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		
		DoublyLinkedList<VOTrip> seleccionados=new DoublyLinkedList<VOTrip>();
		
		Iterator<VOTrip> iterador= tripsList.iterator();
		while(iterador.hasNext()) {
			VOTrip actual=iterador.next();
			if(actual.getToStation().darId()==stationID) {
				seleccionados.add(actual);
			}
		}
		
		return seleccionados;
	}


	@Override
	public void loadServices(String stationsFile) {
		File archivo=new File(stationsFile);
		try {
		FileReader fr=new FileReader(archivo);
		BufferedReader bf=new BufferedReader(fr);
		bf.readLine();
		String datos=bf.readLine();
		while(datos!=null) {
			String[] info=datos.split(",");
			int id=Integer.parseInt(info[0]);
			String name=info[1];
			String city=info[2];
			double latitude=Double.parseDouble(info[3]);
			double longitude=Double.parseDouble(info[4]);
			int capacity=Integer.parseInt(info[5]);
			Date date=new Date(info[6]);
			VOStation nuevo=new VOStation(id, name, city, latitude, longitude, capacity, date);
			stationsList.add(nuevo);
			datos=bf.readLine();
		}
		fr.close();
		bf.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}


}
