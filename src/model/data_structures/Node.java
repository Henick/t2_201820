package model.data_structures;

/**
 * Clase que es la estructura principal de las listas, cada nodo debe tener un elemento
 * @author Nicolas
 *
 */
public class Node<T> {
	/**
	 * Atributo que declara el Elemento que pertenece al nodo
	 */
	private T elemento;
	
	/**
	 * Atributo que contiene el siguiente nodo en la lista
	 */
	private Node<T> siguiente;
	
	/**
	 * Atributo que contiene el anterior nodo en la lista
	 */
	private Node<T> anterior;
	
	/**
	 * Constructor de la clase recibe el elemento que debe contener el nodo
	 * @param elementop
	 */
	public Node (T elementop) {
		elemento=elementop;
	}
	
	public T darElemento() {
		return elemento;
	}
	
	/**
	 * Metodo que retorna el siguiente nodo
	 * @return siguiente
	 */
	public Node<T> darSiguiente(){
		return siguiente;
	}
	
	/**
	 * Metodo que retorna el anterior Nodo
	 * @return anterior
	 */
	public Node<T> darAnterior(){
		return anterior;
	}
	
	/**
	 * Cambia el siguiente nodo
	 * @param nuevo nodo que va a quedar como el siguiente
	 */
	public void cambiarSiguiente(Node<T> nuevo) {
		siguiente=nuevo;
	}
	
	/**
	 * Cambia el anterior nodo
	 * @param nuevo nodo que va a quedar como el anterior
	 */
	public void cambiarAnterior(Node<T> nuevo) {
		anterior=nuevo;
	}
	
}
