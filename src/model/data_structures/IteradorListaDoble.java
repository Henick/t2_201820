package model.data_structures;

import java.util.Iterator;

public class IteradorListaDoble<E> implements Iterator<E>{

	Node<E> actual;
	
	public IteradorListaDoble(Node<E> primero) {
		actual=primero;
	}
	
	@Override
	public boolean hasNext() {
		return actual!=null;
	}

	@Override
	public E next() {
		E elemento=actual.darElemento();
		actual=actual.darSiguiente();
		return elemento;
	}
	
	public boolean hasPrevious() {
		return actual.darAnterior()!=null;
	}

	public E previous() {
		actual=actual.darAnterior();
		return actual.darElemento();
	}

}
