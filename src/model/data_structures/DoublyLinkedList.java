package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList<T> implements IDoublyLinkedList<T> {

	Node<T> primero;
	
	int tamano;
	
	public DoublyLinkedList() {
		tamano=0;
	}
	/**
	 * Adiciona el elemetno dado al final de la lista
	 * @param elemento elemento que se incluira en la lista
	 */
	public boolean add(T elemento) {
		boolean anadido=false;
		Node<T> nuevo=new Node<T>(elemento);
		Node<T> actual=primero;
		if(actual==null) {
			primero=nuevo;
			tamano=1;
			anadido=true;
		}else {
			while(actual.darSiguiente()!=null) {
				actual=actual.darSiguiente();
			}
			actual.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(actual);
			tamano++;
			anadido=true;
		}
		return anadido;
	}
	/**
	 * Elimina el elemento dado por parametro. Si el elmento no esta en la lista no hace nada
	 * @param elemento elemento a ser eliminado
	 */
	public boolean delete(T elemento) {
		boolean eliminado=false;
		Node<T> actual=primero;
		Node<T> anterior=null;
		if(actual.darElemento().equals(elemento)) {
			primero=actual.darSiguiente();
			tamano--;
			eliminado=true;
		}
		while(!actual.darElemento().equals(elemento)&&actual!=null) {
			anterior=actual;
			actual=actual.darSiguiente();
		}
		if(actual==null) {
			
		}else {
			Node<T> elSiguiente=actual.darSiguiente();
			anterior.cambiarSiguiente(elSiguiente);
			elSiguiente.cambiarAnterior(anterior);
			tamano--;
			eliminado=true;
		}
		return eliminado;
	}
	
	@Override
	public int getSize() {
		return tamano;
	}

	@Override
	public Iterator<T> iterator() {
		Iterator<T> iterador = new IteradorListaDoble<T>(primero);
		return iterador;
	}

}
